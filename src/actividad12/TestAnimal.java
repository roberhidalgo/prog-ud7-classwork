package actividad12;

import actividad8.types.animales.Animal;
import actividad8.types.animales.Perro;
import actividad8.types.animales.Leon;
import actividad8.types.animales.Tigre;

public class TestAnimal {

    private static final int MAX_ANIMALES = 10;

    public static void main(String[] args) {

        Animal[] animales = new Animal[MAX_ANIMALES];
        animales[0] = new Tigre("Zimbawe");
        animales[1] = new Tigre("Malasia");
        animales[2] = new Perro("Madrid");
        animales[3] = new Leon("Turquía");
        animales[4] = new Tigre("Marruecos");
        animales[5] = new Tigre("Malasia");
        animales[6] = new Perro("Barcelona");
        animales[7] = new Leon("Turquía");
        animales[8] = new Perro("China");
        animales[9] = new Leon("Turquía");

        Veterinario veterinario = new Veterinario();

        System.out.println("---- Antes de vacunarlos y alimentarlos ------");
        for (Animal animal: animales) {
            System.out.println(animal);
        }

        for (Animal animal: animales) {
            veterinario.inocular(animal);
            veterinario.alimentar(animal);
        }

        System.out.println("---- Después de vacunarlos y alimentarlos ------");
        for (Animal animal: animales) {
            System.out.println(animal);
        }
    }
}
