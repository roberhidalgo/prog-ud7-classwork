package actividad12.types.animales;

public abstract class Animal {

    private boolean vacunado;
    public enum TipoComida {HERBIVORO, CARNIVORO, OMNIVORO};
    private TipoComida comida;
    private int hambre;
    public enum Tamanyo {PEQUEÑO, MEDIANO, GRANDE};
    private Tamanyo tamanyo;
    private String localizacion;

    public Animal(TipoComida comida, Tamanyo tamanyo, String localizacion) {
        this.comida = comida;
        this.tamanyo = tamanyo;
        this.localizacion = localizacion;
        this.vacunado = false;
        this.hambre = 8;
    }

    public abstract void emitirSonido();

    public abstract String getNombre();

    public void comer() {
        System.out.println("Comiendo un " + this.getNombre() + "....");
        switch(this.comida) {
            case OMNIVORO:
                this.hambre -= 3;
                break;
            case HERBIVORO:
                this.hambre -= 1;
                break;
            case CARNIVORO:
                this.hambre -= 2;
                break;
            default:
                System.out.println("Error");
        }
    }

    public void vacunar() {
        System.out.println("Vacunando...");
        this.vacunado = true;
        this.emitirSonido();
    }

    @Override
    public String toString() {
        return this.getNombre() + ": Tamaño=" + tamanyo +
                ",Nivel de hambre=" + hambre +
                ",vacunado=" + vacunado +
                ",vive en='" + localizacion;
    }
}
