package actividad12.types.animales;

import actividad12.types.animales.Animal;

public class Hipopotamo extends Animal {
    public Hipopotamo(String localizacion) {
        super(TipoComida.OMNIVORO, Tamanyo.GRANDE, localizacion);
    }



    @Override
    public void emitirSonido() {
        System.out.println("HIPPPP!!!!!!!!!");
    }

    @Override
    public String getNombre() {
        return "Hipopótamo";
    }
}
