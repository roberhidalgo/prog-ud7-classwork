package actividad12.types;

import actividad12.Mascota;

public class RoboPerro implements Mascota {
    @Override
    public void serAmigable() {
        System.out.println("Encendiendo la tele");
    }

    @Override
    public void jugar() {
        System.out.println("Tira una tuerca");
    }
}
