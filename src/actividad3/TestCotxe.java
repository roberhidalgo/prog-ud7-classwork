package actividad3;

import actividad3.vehiculos.Cotxe;
import actividad3.vehiculos.CotxeEsportiu;

public class TestCotxe {
    public static void main(String[] args) {

        Cotxe coche = new Cotxe(
                0, 0, "AAAA");
        CotxeEsportiu cocheEsportiu = new CotxeEsportiu(
                0, 0, "BBBB", true);

        coche.repostar(50);
        cocheEsportiu.repostar(50);

        for (int i = 0; i < 5; i++) {
            coche.accelerar();
            cocheEsportiu.accelerar();
        }
        coche.frenar();
        cocheEsportiu.frenar();


        System.out.println(coche);
        System.out.println(cocheEsportiu);
    }
}
