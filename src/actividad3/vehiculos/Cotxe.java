package actividad3.vehiculos;

public class Cotxe extends Vehicle {

    protected float carburant;
    private String matricula;

    public Cotxe(int velocitat, float carburant, String matricula) {
        super(velocitat);
        this.carburant = carburant;
        this.matricula = matricula;
    }

    @Override
    public void accelerar() {
        super.accelerar();
        carburant -= 0.5;
    }

    public void repostar(int quantitat) {
        carburant += quantitat;
    }

    @Override
    public String toString() {
        return this.matricula
                + " Carburante: " + this.carburant;
    }
}