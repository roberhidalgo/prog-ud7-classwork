package actividad3.vehiculos;

public class CotxeEsportiu extends Cotxe{
    private boolean esDescapotable;

    public CotxeEsportiu(
            int velocitat, float carburant,
            String matricula, boolean esDescapotable) {
        super(velocitat, carburant, matricula);
        this.esDescapotable = esDescapotable;
    }

    @Override
    public void accelerar() {
        super.accelerar();
        this.carburant -= 1;
    }

    @Override
    public String toString() {
        String textoDescapotable = "";
        if (this.esDescapotable) {
            textoDescapotable += "(Descapotable)";
        }
        return super.toString() + " " + textoDescapotable;
    }
}
