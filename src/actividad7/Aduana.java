package actividad7;

import actividad7.personas.Persona;
import actividad7.personas.PersonaInglesa;

public class Aduana {

    private static final int MAX_PERSONAS = 10;

    private Persona[] personas;

    public Aduana() {
        this.personas = new Persona[MAX_PERSONAS];
    }

    public void entrar(Persona persona) {
        System.out.println("You're welcome");
        for (int i = 0; i < this.personas.length; i++) {

            if (this.personas[i] == null) {
                persona.saluda();

                if (persona instanceof PersonaInglesa) {
                    System.out.println("Could you show me your passport, please?");
                    PersonaInglesa persIng = (PersonaInglesa) persona;
                    persIng.mostrarPasaporte();
                }
                return;
            }
        }

        System.out.println("Sorry, the queue is full!!");
    }
}
