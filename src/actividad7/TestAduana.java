package actividad7;

import actividad7.personas.PersonaRusa;
import actividad7.Aduana;
import actividad7.personas.Persona;
import actividad7.personas.PersonaFrancesa;
import actividad7.personas.PersonaInglesa;

public class TestAduana {
    public static void main(String[] args) {
        Persona persona = new Persona("Pedro Martínez");
        PersonaInglesa ingles = new PersonaInglesa("Peter York", "1234A");
        PersonaFrancesa frances = new PersonaFrancesa("Zinedine Zidane");
        PersonaRusa ruso = new PersonaRusa("Vladimir Estlovski");

        Aduana aduana = new Aduana();
        aduana.entrar(ruso);
        aduana.entrar(persona);
        aduana.entrar(frances);
        aduana.entrar(ingles);

    }
}
