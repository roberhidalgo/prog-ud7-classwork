package actividad7.personas;

public class PersonaInglesa extends Persona {

    private String pasaporte;

    public PersonaInglesa(String nombre, String pasaporte) {
        super(nombre);
        this.pasaporte = pasaporte;
    }

    @Override
    public void saluda() {
        System.out.println("Hello, how are you?. My name is " +  nombre);
    }

    public void mostrarPasaporte() {
        System.out.println("My passport is " + this.pasaporte);
    }
}
