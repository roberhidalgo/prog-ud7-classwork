package actividad10;

public class Vendedor {

    private static final int MAX_CLIENTES = 10;

    private Cliente[] clientes;

    public Vendedor() {
        this.clientes = new Cliente[MAX_CLIENTES];
    }

    public void anyadir(Cliente cliente) {

        if (!existe(cliente)){
            for (int i = 0; i < this.clientes.length; i++) {
                if (this.clientes[i] == null) {
                    this.clientes[i] = cliente;
                    return;
                }
            }

            System.out.println("NO se pudo añadir al cliente " + cliente + ". Lista llena");
        } else {
            System.out.println("NO se pudo añadir al cliente " +  cliente + ". Ya existe");
        }
    }

    private boolean existe(Cliente cliente) {
        for (int i = 0; i < this.clientes.length; i++) {
            if (this.clientes[i].equals(cliente)) {
                return true;
            }
        }

        return false;
    }
}
