package actividad10;

public class Cliente {

    private String nombre;
    private String direccion;

    public Cliente(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Cliente)) {
            return false;
        }

        return ((Cliente)obj).nombre.equals(this.nombre);
    }

    @Override
    public String toString() {
        return nombre;
    }
}
