package actividad8.types.animales;

import actividad8.types.animales.Animal;

public class Tigre extends Animal {
    public Tigre(String localizacion) {
        super(TipoComida.CARNIVORO, Tamanyo.GRANDE, localizacion);
    }

    @Override
    public void emitirSonido() {
        System.out.println("OAUGGGGGGG....!");
    }

    @Override
    public String getNombre() {
        return "Tigre";
    }
}
