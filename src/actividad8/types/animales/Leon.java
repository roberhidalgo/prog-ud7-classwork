package actividad8.types.animales;

import actividad8.types.animales.Animal;

public class Leon extends Animal {

    public Leon(String localizacion) {
        super(TipoComida.CARNIVORO, Tamanyo.GRANDE, localizacion);
    }

    @Override
    public void emitirSonido() {
        System.out.println("ARGHHHHHHHH!!!!!!!!!");
    }

    @Override
    public String getNombre() {
        return "León";
    }
}
