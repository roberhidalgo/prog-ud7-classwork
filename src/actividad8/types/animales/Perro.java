package actividad8.types.animales;

import actividad8.types.animales.Animal;

public class Perro extends Animal {
    public Perro(String localizacion) {
        super(TipoComida.CARNIVORO, Tamanyo.MEDIANO, localizacion);
    }

    @Override
    public void emitirSonido() {
        System.out.println("GRRRRRRR!!!!!!!!!");
    }

    @Override
    public String getNombre() {
        return "Perro";
    }
}
