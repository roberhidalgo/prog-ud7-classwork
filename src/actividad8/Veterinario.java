package actividad8;

import actividad8.types.animales.Animal;
import actividad8.types.animales.Leon;
import actividad8.types.animales.Tigre;

public class Veterinario {

    public void alimentar(Animal animal) {
        animal.comer();
    }

    public void inocular(Animal animal) {

        if (animal instanceof Tigre
                || animal instanceof Leon) {
            animal.comer();
        }

        animal.vacunar();
    }
}
