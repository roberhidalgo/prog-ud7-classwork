package actividad1;

import actividad1.personas.Persona;
import actividad1.personas.PersonaFrancesa;
import actividad1.personas.PersonaInglesa;
import actividad1.personas.PersonaRusa;

public class TestPersona {
    public static void main(String[] args) {
        Persona español = new Persona("Roberto");
        PersonaInglesa ingles = new PersonaInglesa("Mike");
        PersonaFrancesa frances = new PersonaFrancesa("Janvier");

        español.saluda();
        System.out.println(español.getNombre());
        ingles.saluda();
        System.out.println(ingles.getNombre());
        frances.saluda();
        System.out.println(frances.getNombre());


    }
}
