package actividad1.personas;

public class PersonaRusa extends Persona {

    public PersonaRusa(String nombre) {
        super(nombre);
    }

    @Override
    public void saluda() {
        System.out.println("Haloksi, com estoki. Mi nombroski es " + nombre);
    }
}
