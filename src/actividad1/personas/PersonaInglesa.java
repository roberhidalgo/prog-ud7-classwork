package actividad1.personas;

public class PersonaInglesa extends Persona {

    public PersonaInglesa(String nombre) {
        super(nombre);
    }

    @Override
    public void saluda() {
        System.out.println("Hello, how are you?. My name is " +  nombre);
    }

}
