package actividad1.personas;

public class PersonaFrancesa extends Persona {

    public PersonaFrancesa(String nombre) {
        super(nombre);
    }

    @Override
    public void saluda() {
        System.out.println("Bon jour, je suis " + this.nombre);
    }
}
