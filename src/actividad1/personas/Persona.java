package actividad1.personas;

public class Persona {

    protected String nombre;

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public void saluda() {
        System.out.println("Hola qué tal, me llamo " + nombre);
    }

    public String getNombre() {
        return this.nombre;
    }
}
