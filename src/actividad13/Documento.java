package actividad13;

public class Documento implements Imprimible{

    private String titulo;
    private String fecha;
    private String texto;

    public Documento(String titulo, String fecha, String texto) {
        this.titulo = titulo;
        this.fecha = fecha;
        this.texto = texto;
    }

    @Override
    public String obtenerTexto() {
        return "Título: " + titulo
                + " Fecha: " + fecha
                + " Texto: " + texto;
    }
}
