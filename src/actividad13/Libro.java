package actividad13;

public class Libro extends Material implements Imprimible{

    private int numPaginas;
    private String capituloMuestra;

    public Libro(int codigo, String titulo, String autor, int numPaginas, String capituloMuestra) {
        super(codigo, titulo, autor);
        this.numPaginas = numPaginas;
        this.capituloMuestra = capituloMuestra;
    }

    @Override
    public String obtenerTexto() {
        return "[LIBRO]: " +
                "Título: " + this.titulo +
                "Autor: " + this.autor +
                "Cap. Muestra: " + this.capituloMuestra;
    }
}
