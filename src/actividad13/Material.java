package actividad13;

public abstract class Material {
    private int codigo;
    protected String titulo;
    protected String autor;

    public Material(int codigo, String titulo, String autor) {
        this.codigo = codigo;
        this.titulo = titulo;
        this.autor = autor;
    }
}
