package actividad13;

public class Impresora {

    private String marca;
    private String modelo;
    private int numSerie;

    public Impresora(String marca, String modelo, int numSerie) {
        this.marca = marca;
        this.modelo = modelo;
        this.numSerie = numSerie;
    }

    public void imprimir(Imprimible imprimible) {

        System.out.println(imprimible.obtenerTexto());
    }
}
