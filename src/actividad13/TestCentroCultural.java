package actividad13;

public class TestCentroCultural {

    public static void main(String[] args) {
        Documento documento1 = new Documento("Normas Préstamo libros",
                "23-02-2020", "Lorem Ipsum is\n" +
                "simply dummy text of the printing and typesetting industry.");
        Libro libro1 = new Libro(1, "La madre de Frankenstein", "Anónimo",
                200, "Capítulo introducción");

        Impresora impresora = new Impresora("HP", "32432", 123432);
        impresora.imprimir(documento1);
        impresora.imprimir(libro1);
    }
}
